<!DOCTYPE html>
<html>
<meta charset="UTF-8">
<link rel="stylesheet" href="index.css" type="text/css">

<body>

    <?php
        $gender = array(0 => 'Nam', 1 => 'Nữ');
        $khoas = array(""=>'',"MAT" => 'Khoa học máy tính', "KDL" => 'Khoa học dữ liệu');
        $notifi = array();
        if (!empty($_POST["submit"])){
            $notifi['tên'] = (!empty($_POST["name"])) ? 1 : 0;
            $notifi['giới tính'] = (!empty($_POST["gender"])) ? 1 : 0;
            $notifi['phân khoa'] = (!empty($_POST["khoa"])) ? 1 : 0;
            $adnotifid['ngày sinh'] = (!empty($_POST["date"])) ? 1 : 0;
        };
    ?>

    <div class="login">
        <div>
            <?php 
                if (!empty($_POST["submit"])){
                    foreach ($notifi as $key => $value){
                        if ($value == 0){
                            echo '<div class="div_notfi" style="color:crimson">Hãy nhập '.$key.'.</div>';
                        }
                    };
                }
            ?>
        </div>

        <form method="POST" action="index.php">
            <div class="row">
                <div class="column_label div_label">Họ và tên<sapn style="color:crimson">*</sapn></div>
                <div class="column"> </div>
                <div class="column_input">
                    <input class="div_input" name="name">
                </div>
            </div>

            <div class="row">
                <div class="column_label div_label">Giới tính<sapn style="color:crimson">*</sapn></div>
                <div class="column"> </div>
                <div class="column_input">
                    <div class="div_radio row_radio">
                        <?php
                        for ($i = 0; $i < count($gender); $i++) {
                            echo '<input class = "input_radio" type="radio" name="gender" value="'.$gender[$i].'" >
                            <label class = "radio"  for="html">' . $gender[$i] . '</label><br>';
                        };
                        ?>
                    </div>
                </div>
            </div>

            <div class="row">
                <div class="column_label div_label">Phân khoa<sapn style="color:crimson">*</sapn></div>
                <div class="column"> </div>
                <div class="column_input">
                    <select class="div_select" name="khoa" id="khoa">
                        <?php
                            foreach ($khoas as $khoa) {
                                echo '<option value="' . $khoa . '" name = "khoa">' . $khoa . '</option>';
                            };
                        ?>
                    </select>
                </div>
            </div>

            <div class="row">
                <div class="column_label div_label">Ngày sinh<sapn style="color:crimson">*</sapn></div>
                <div class="column"> </div>
                <div class="column_input">
                    <input class="input_date" name = "date" required type="date" placeholder="dd/mm/yyyy" />
                </div>
            </div>

            <div class="row">
                <div class="column_label div_label">Địa chỉ</div>
                <div class="column"> </div>
                <div class="column_input">
                    <input name = "address" class="div_input ">
                </div>
            </div>

            <div class="">
                <input class="center div_button" type="submit" value="Đăng ký" name="submit">
            </div>

        </form>

    </div>
</body>